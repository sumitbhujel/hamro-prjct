package model;

public class Manager extends User {

    public Manager(String userId, String name, String contact, String address, String type, String username) {
        super(userId, name, contact, address, type, username);
    }

    public void approve(Incident in) {
        in.setManagerId(super.getUserId());
        in.setIsApproved(Status.approved.toString());
    }

    public void unApprove(Incident in) {
        //an incident cannot be unapproved by a manager if it has been approved by a different manager. we make sure of that in this if condition
        if (in.getIsApproved() == Status.pending.toString() || in.getManagerId() == super.getUserId()){
            in.setIsApproved(Status.unapproved.toString());
        }
    }
}
