package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class IncidentsController extends Controller implements Initializable {

    @FXML Label labelUserName = new Label();
    @FXML Label labelUserLoginTime = new Label();
    @FXML TextField tfSearch = new TextField();
    @FXML Button btnSearch = new Button();
    @FXML Button btnCreateIncident = new Button();
    @FXML Button btnHome = new Button();

    @Override
    public void initialize(URL location, ResourceBundle resources) {


        btnHome.setOnAction(event -> {
            goToDashboard(btnHome);
        });
    }
}
