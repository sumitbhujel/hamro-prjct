package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class DashboardController extends Controller implements Initializable {

    @FXML Label labelUserName = new Label();
    @FXML Label labelLoginTime = new Label();

    @FXML Label labelGreeting = new Label();

    @FXML Label labelNotes = new Label();
    @FXML Label labelIncidents = new Label();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setUpLabels();
    }

    public void setUpLabels(){
        labelUserName.setText("USERNAME");//need username from currentUser object
        labelLoginTime.setText("last login at:");//need to concatenate date
        labelGreeting.setText("WELCOME " + "" + ",");//need username from currentUser object

        setupLabelsListener(labelNotes, "notes");
        setupLabelsListener(labelIncidents, "incidents");

    }

    public void setupLabelsListener(Label label, String fxmlFile) {
        label.setOnMouseClicked(event -> {
                goToScreen(label, fxmlFile);//from parent class Controller
        });

    }


}
