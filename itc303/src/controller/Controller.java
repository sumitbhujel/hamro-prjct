package controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

public abstract class Controller {
    //this class is created because almost all controller need to
    // keep changing screens and the codes for changing screens is reused
    //several times. after extending this class they will all inherit the
    //common behaviour of going to a different screen. now all they need to do is call
    //a method

    public void goToDashboard(Node context){
        goToScreen(context, "dashboard");
    }

    public void goToScreen(Node context, String fxmlFile){//context is not really context but the component in the current gives idea of where it is currently in
        try {
            VBox dashboard = FXMLLoader.load(getClass().getResource("../view/" + fxmlFile + ".fxml"));

            //load the dashboard screen
            Stage newStage = (Stage) context.getScene().getWindow();
            newStage.setScene(new Scene(dashboard, 1000, 700));
            newStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
