package model;

public class Staff extends User {
    private String managerId;

    public Staff(String userId, String name, String contact, String address, String type, String managerId, String username) {
        super(userId, name, contact, address, type, username);
        this.managerId = managerId;
    }


}
