package controller;

import javafx.animation.PauseTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;

import javafx.util.Duration;
import model.Incident;
import model.Manager;
import model.Staff;
import model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.sql.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class PendingIncidentsController implements Initializable {
    HttpConnector httpConnector = new HttpConnector();

    @FXML private TableView incidentsTable = new TableView();
    @FXML private GridPane incidentView = new GridPane();
    @FXML private Label labelUsername = new Label();

    ObservableList<Incident> unapprovedIncidentsList = FXCollections.observableArrayList();//a list that contains the student objects

    private Statement stmt;
    private User currentUser;

    @Override
    public void initialize(URL location, ResourceBundle resources){

        try {
            JSONObject currentUserJson = getCurrentUser();
            if(currentUserJson.getString("s_type") == "manager"){
                currentUser = new Manager(currentUserJson.getString("s_id"), currentUserJson.getString("s_name"), currentUserJson.getString("s_contact"), currentUserJson.getString("s_address"), currentUserJson.getString("s_type"), currentUserJson.getString("username"));
            }
            else {
                currentUser = new Staff(currentUserJson.getString("s_id"), currentUserJson.getString("s_name"), currentUserJson.getString("s_contact"), currentUserJson.getString("s_address"), currentUserJson.getString("s_type"), currentUserJson.getString("manager"), currentUserJson.getString("username") );
            }

            labelUsername.setText(" " + currentUser.getUsername());
            listPendingIncidents();

        }  catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        setupTable();
        setUpTableListener();



    }

    public void setupTable(){
        TableColumn colIncidentId = new TableColumn("Incident Id");
        TableColumn colDate = new TableColumn("Date");
        TableColumn colTime = new TableColumn("Time");
        TableColumn colType = new TableColumn("Incident Type");
        TableColumn colDesc = new TableColumn("Description");
        TableColumn colStaff = new TableColumn("Staff Id");
        TableColumn colClient = new TableColumn("Client Id");
        TableColumn colStatus = new TableColumn("Status");

        incidentsTable.getColumns().addAll(colIncidentId, colDate, colTime, colType, colDesc, colStaff, colClient, colStatus);

        colIncidentId.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.08));
        colDate.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.1));
        colTime.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.08));
        colType.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.1));
        colDesc.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.40));
        colStaff.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.08));
        colClient.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.08));
        colStatus.prefWidthProperty().bind(incidentsTable.widthProperty().multiply(0.08));

        colIncidentId.setCellValueFactory(new PropertyValueFactory<Incident, String>("incidentId"));
        colDate.setCellValueFactory(new PropertyValueFactory<Incident, String>("incidentDate"));
        colTime.setCellValueFactory(new PropertyValueFactory<Incident, String>("incidentTime"));
        colType.setCellValueFactory(new PropertyValueFactory<Incident, String>("type"));
        colDesc.setCellValueFactory(new PropertyValueFactory<Incident, String>("detail"));
        colStatus.setCellValueFactory(new PropertyValueFactory<Incident, String>("isApproved"));
        colStaff.setCellValueFactory(new PropertyValueFactory<Incident, String>("staffId"));
        colClient.setCellValueFactory(new PropertyValueFactory<Incident, String>("clientId"));

        //root.getChildren().add(incidentsTable);
    }

    public void setUpTableListener(){
        incidentsTable.setRowFactory( tv -> {
            TableRow<Incident> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Incident rowData = row.getItem();
                    System.out.println("Status is: " + rowData.getIsApproved() + ".");
                    viewIncident(rowData);
                }
            });
            return row ;
        });
    }

    public void viewIncident(Incident in){
        incidentView.getChildren().clear();

        Label labelType = new Label(in.getType());
        Label labelStatus = new Label(in.getIsApproved());

        Label labelTxtIncidentId = new Label("Incident No: ");
        Label labelIncidentId = new Label(String.valueOf(in.getIncidentId()));

        Label labelIncidentDate = new Label(in.getIncidentDate());
        Label labelIncidentTime = new Label(in.getIncidentTime());

        Label labelTextStaff = new Label("Reported by(client id): ");
        Label labelTextClient = new Label("Client(s) involved: ");

        Label labelStaff = new Label(String.valueOf(in.getStaffId()));
        Label labelClient = new Label(String.valueOf(in.getClientId()));

        Label labelDetail = new Label(in.getDetail());

        Label labelAttention = new Label("This incident needs manager's attention");

        ComboBox combo = new ComboBox();
        combo.getItems().addAll("Approve", "Unapprove");

        Button btnSave = new Button("Save");

        Label msg = new Label();

        //this block is for making the label visible for few seconds
        PauseTransition visiblePause = new PauseTransition(
                Duration.seconds(4)
        );
        visiblePause.setOnFinished(
                event -> msg.setVisible(false)
        );
        //end

        btnSave.setOnAction(event -> {
            String selection = "";
            String query;


            if(combo.getValue() == null){
                incidentView.add(new Label("Please select an option"), 1, 7);
            }
            else{
                if(combo.getValue().equals("Approve")){
                    System.out.println("Approve is selected");
                    msg.setText("Incident is successfully updated");
                    selection = "approved";
                }
                else if(combo.getValue().equals("Unapprove")){
                    System.out.println("unApprove is selected");
                    msg.setText("Incident is successfully updated");
                    selection = "unapproved";
                }
                try {
                    httpConnector.postNoJSON("updateIncident.php", "in_id=" + in.getIncidentId() + "&is_approved=" + selection + "&manager=" + currentUser.getUserId());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    listPendingIncidents();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                incidentView.getChildren().clear();
                incidentView.getChildren().add(msg);
                visiblePause.play();
            }

        });


        incidentView.add(labelStatus, 0, 0);
        incidentView.add(labelType, 1, 0);
        incidentView.add(labelTxtIncidentId, 0, 1);
        incidentView.add(labelIncidentId, 1, 1);
        incidentView.add(labelIncidentDate, 0, 2);
        incidentView.add(labelIncidentTime, 1, 2);
        incidentView.add(labelTextStaff, 0, 3);
        incidentView.add(labelTextClient, 1, 3);
        incidentView.add(labelStaff, 0, 4);
        incidentView.add(labelClient, 1, 4);
        incidentView.add(labelDetail, 0, 5, 2, 1);

        if(in.getIsApproved().equals("pending")){
            incidentView.add(labelAttention, 0, 6, 2, 1);
            incidentView.add(combo, 0, 7);
            incidentView.add(btnSave, 0, 8);
        }

        incidentView.setVgap(20);
        incidentView.setHgap(20);
    }

    private JSONObject getCurrentUser() throws IOException, JSONException {
        JSONObject user = null;
        JSONObject resultObject = httpConnector.postWithParams("getUser.php",  "s_id=1");
        JSONArray jArray = resultObject.getJSONArray("user");
        for(int i = 0; i < 1; ++i){
            user = jArray.getJSONObject(i);
        }

        return user;
    }

    public void listPendingIncidents() throws IOException, JSONException {//adds data and displays on the tableview.
        incidentsTable.getItems().clear();

        JSONObject pendingIncidentsJson = httpConnector.post("getPendingIncidents.php");

        JSONArray jsonArray = pendingIncidentsJson.getJSONArray("pendingIncidents");

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject j = jsonArray.getJSONObject(i);
            Incident in = new Incident(
                    j.getInt("in_id"),
                    j.getString("in_time"),
                    j.getString("in_date"),
                    j.getString("in_type"),
                    j.getString("in_detail"),
                    j.getString("is_approved"),
                    j.getString("client"),
                    j.getString("staff"),
                    j.getString("manager"));
            unapprovedIncidentsList.add(in);
        }


        incidentsTable.setItems(unapprovedIncidentsList);//list is added to TableView
    }

}
