package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController extends Controller implements Initializable {
    @FXML TextField tfUserName = new TextField();
    @FXML PasswordField pfPasswordField = new PasswordField();
    @FXML Button btnLogin = new Button();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        setupLoginButtonListener();


    }

    public void setupLoginButtonListener()
    {
        btnLogin.setOnAction(event -> {
//            VBox dashboard = null;
//            try {
//                dashboard = FXMLLoader.load(getClass().getResource("../view/dashboard.fxml"));
//            } catch (IOException e) {
//                System.out.println("dashboard is still null. THERE IS SOME ERROR");
//                e.printStackTrace();
//            }

            String userID  = login(tfUserName.getText(), pfPasswordField.getText());

            if(!userID.equals("-1")){//if login unsuccessful
                System.out.println("login successful");

                //load the dashboard screen
//                Stage newStage = (Stage) btnLogin.getScene().getWindow();
//                newStage.setScene(new Scene(dashboard, 1000, 700));
//                newStage.show();

                goToDashboard(btnLogin);
            }
            else {
                System.out.println("login failed");
            }
        });
    }

    private String login(String userName, String password)
    {
        //still need to implement login logic
        HttpConnector login = new HttpConnector();
        JSONObject loginStatus;
        String loggedIn = null;
        try {
            loginStatus = login.postWithParams("login.php", "username=" + userName + "&password=" + password);
            loggedIn = loginStatus.get("user").toString();
        }
        catch (Exception e)
        {
            System.out.println("Exception occured in login function: "+ e.toString());
        }

        return loggedIn;
    }
}
